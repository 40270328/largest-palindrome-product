﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Largest_Palindrome_product
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public static string reverse(String s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new String(charArray);
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            int num1 = 100, num2 = 100, sum = 0, palindrome = 0;
            String firstNum = "", numCheck = "", answer = "";
            Boolean exitCheck = false;

            for (num1 = 100; num1 <= 999; num1++)
            {
                for(num2 = 100; num2 <= 999; num2++)
                {
                    sum = num1 * num2;

                    firstNum = sum.ToString();
                    numCheck = reverse(firstNum);

                    if (firstNum.Equals(numCheck) && sum > palindrome)
                    {
                        palindrome = sum;

                        answer = num1 + " * " + num2;
                    }
                }
            }

            lblAnswer.Content = "Answer: " + answer + " = " + palindrome;
        }

    }

       
}
